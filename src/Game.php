<?php

namespace Bowling;

Class Game
{
    private const STRIKE = 'X';
    private const SPARE = '/';

    private $roundsPlayed;
    private $frames = [];

    function __construct(int $rounds){
        $this->rounds = $rounds;
    }

    public function getRounds(): int
    {
        return $this->rounds;
    }

    public function getScore(): int
    {
        $runningTotal = 0;
        foreach ($this->frames as $frameCounter => $frame) {
            $runningTotal += $this->getFrameScore($frameCounter);
        }

        // TODO: Handle the last frame where 3 bowls is possible
        // TODO: Handle double and triple strikes etc.
        return $runningTotal;
    }

    private function getFrameScore(int $frameKey): int
    {
        $frame = $this->frames[$frameKey];
        $nextFrameKey = $frameKey + 1;

        // Handle strikes
        if ($frame[0] === self::STRIKE) {
            if ($nextFrameKey !== $this->getRounds()) {
                return 10 + array_sum($this->frames[$frameKey + 1]);
            } else {
                return 10;
            }
        } elseif ($frame[1] === self::SPARE) {
            if ($nextFrameKey === $this->getRounds()) {
                return 10 + $frame[2];
            }
            if ($nextFrameKey !== $this->getRounds()) {
                return 10 + $this->frames[$frameKey + 1][0];
            } else {
                return 10;
            }
        }

        return array_sum($frame);
    }

    public function addFrame(string $round): void
    {
        if ($round === self::STRIKE) {
            $this->frames[] = [self::STRIKE];
        } elseif (strlen($round) === 3) {
            $this->frames[] = [$round[0], $round[1], $round[2]];
        } else {
            $this->frames[] = [$round[0], $round[1]];
        }
    }
}

