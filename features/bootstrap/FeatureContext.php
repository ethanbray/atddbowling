<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;


/**
 * you can have a __construct function
 * for more information also visit https://docs.behat.org/en/v2.5/guides/3.hooks.html
 */

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    /**
     * @var \Bowling\Game
     */
    private $bowlingGame;
    private $resultingScore;


    /**
     * @Given I play a :rounds rounds Bowling game
     */
    public function iPlayARoundsBowlingGame($rounds)
    {
        $this->bowlingGame = new Bowling\Game($rounds);
    }

    /**
     * @When I bowl a game :arg1 :arg2 :arg3 :arg4 :arg5 :arg6 :arg7 :arg8 :arg9 :arg10
     */
    public function iBowlAGame($arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8, $arg9, $arg10)
    {
        foreach (func_get_args() as $arg) {
            $this->bowlingGame->addFrame($arg);
        }
    }

    /**
     * @Then the game result will be :arg1
     */
    public function theGameResultWillBe($expectedResult)
    {
        if ($expectedResult != $this->bowlingGame->getScore()) {
            throw new RuntimeException(sprintf(
                'Resulting score %s did not match expected result %s',
                $this->bowlingGame->getScore(),
                $expectedResult
            ));
        }

    }
}
