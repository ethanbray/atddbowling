Feature:
ScoreBoard showing the results of our bowling game with spares

@pending
Scenario Outline: Calculate Game Score for Bowling Game with spares
  Given I play a 10 rounds Bowling game
  When I bowl a game <game>
  Then the game result will be <result>
  Examples:
    | game                             | result |
    | X 00 00 00 00 00 00 00 00 00     | 10     |
    | X 50 00 00 00 00 00 00 00 00     | 20     |
    | X 05 00 00 00 00 00 00 00 00     | 20     |

